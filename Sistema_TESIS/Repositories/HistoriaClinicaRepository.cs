﻿

using Microsoft.EntityFrameworkCore;
using Sistema_TESIS.Models;
using Sistema_TESIS.Models.DB;
using System.Linq;


namespace Sistema_TESIS.Repositories
{
    public interface IHistoriaClinicaRepository
    {
        public Persona PersonaConHistoriaClinica(int id);
        public HistoriaClinica FindHistoriaClinicaById(int historiaClinicaId);
    }
    public class HistoriaClinicaRepository : IHistoriaClinicaRepository
    {
        private readonly AppDbContext context;

        public HistoriaClinicaRepository(AppDbContext context)
        {
            this.context = context;
        }

        public Persona PersonaConHistoriaClinica(int id)
        {
            var model = context.Personas.Include(o => o.HistoriaClinica).FirstOrDefault(o => o.PersonaId == id);

            return model;
        }
        public HistoriaClinica FindHistoriaClinicaById(int historiaClinicaId)
        {
            var model = context.HistoriaClinicas.FirstOrDefault(o => o.HistoriaClinicaId == historiaClinicaId);
            return model;
        }
    }
}
