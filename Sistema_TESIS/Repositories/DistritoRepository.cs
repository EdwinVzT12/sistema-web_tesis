﻿
using Sistema_TESIS.Models.DB;
using System.Collections.Generic;
using System.Linq;


namespace Sistema_TESIS.Repositories
{
    public interface IDistritoRepository
    {
        public List<Distrito> ListaDistritosBD();
    }
    public class DistritoRepository : IDistritoRepository
    {
        public readonly AppDbContext context;
        public DistritoRepository(AppDbContext context)
        {
            this.context = context;
        }

        public List<Distrito> ListaDistritosBD()
        {
            return context.Distritos.ToList();
        }
    }
}
