#pragma checksum "C:\Users\cocad.EDWIN\Downloads\Sistema_TESIS\Sistema_TESIS\Views\Receta\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "a281e00dbccfa2d596abf600b9ecf18100b3ccbe"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Receta_Index), @"mvc.1.0.view", @"/Views/Receta/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\cocad.EDWIN\Downloads\Sistema_TESIS\Sistema_TESIS\Views\_ViewImports.cshtml"
using Sistema_TESIS;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\cocad.EDWIN\Downloads\Sistema_TESIS\Sistema_TESIS\Views\_ViewImports.cshtml"
using Sistema_TESIS.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"a281e00dbccfa2d596abf600b9ecf18100b3ccbe", @"/Views/Receta/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"7017122e196eca4cb328ea36ceee3e28a042714d", @"/Views/_ViewImports.cshtml")]
    public class Views_Receta_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 2 "C:\Users\cocad.EDWIN\Downloads\Sistema_TESIS\Sistema_TESIS\Views\Receta\Index.cshtml"
  
    Layout = "_Layout2";
    ViewData["Title"] = "Index";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
<div class=""col-xl-4 text-left my-3 mx-3"">
    <h3 class=""card-title blue-text font-weight-bold"">Recetas</h3>
</div>

<div class=""col-xl-12 my-3"">
    <table class=""table"">
        <thead class=""light-blue text-white"">
            <tr>
                <th scope=""col"">Fecha</th>
                <th scope=""col"">CuadroClinicoId</th>
                <th scope=""col"">Descripcion</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
");
#nullable restore
#line 23 "C:\Users\cocad.EDWIN\Downloads\Sistema_TESIS\Sistema_TESIS\Views\Receta\Index.cshtml"
             foreach (var item in Model)
            {

#line default
#line hidden
#nullable disable
            WriteLiteral("            <tr>\r\n                <th scope=\"row\">");
#nullable restore
#line 26 "C:\Users\cocad.EDWIN\Downloads\Sistema_TESIS\Sistema_TESIS\Views\Receta\Index.cshtml"
                           Write(item.FechaEmision.ToShortDateString());

#line default
#line hidden
#nullable disable
            WriteLiteral("</th>\r\n                <td>");
#nullable restore
#line 27 "C:\Users\cocad.EDWIN\Downloads\Sistema_TESIS\Sistema_TESIS\Views\Receta\Index.cshtml"
               Write(item.CuadroClinico.CuadroClinicoId);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                <td>");
#nullable restore
#line 28 "C:\Users\cocad.EDWIN\Downloads\Sistema_TESIS\Sistema_TESIS\Views\Receta\Index.cshtml"
               Write(item.Descripcion);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                <td><a");
            BeginWriteAttribute("href", " href=\"", 851, "\"", 896, 2);
            WriteAttributeValue("", 858, "/receta/delete?RecetaId=", 858, 24, true);
#nullable restore
#line 29 "C:\Users\cocad.EDWIN\Downloads\Sistema_TESIS\Sistema_TESIS\Views\Receta\Index.cshtml"
WriteAttributeValue("", 882, item.RecetaId, 882, 14, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" class=\"btn btn-outline-danger\">Eliminar</a></td>\r\n                <td><a");
            BeginWriteAttribute("href", " href=\"", 970, "\"", 1015, 2);
            WriteAttributeValue("", 977, "/receta/Update?RecetaId=", 977, 24, true);
#nullable restore
#line 30 "C:\Users\cocad.EDWIN\Downloads\Sistema_TESIS\Sistema_TESIS\Views\Receta\Index.cshtml"
WriteAttributeValue("", 1001, item.RecetaId, 1001, 14, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" class=\"btn btn-outline-blue\">Actualizar</a></td>\r\n            </tr>\r\n");
#nullable restore
#line 32 "C:\Users\cocad.EDWIN\Downloads\Sistema_TESIS\Sistema_TESIS\Views\Receta\Index.cshtml"
            }

#line default
#line hidden
#nullable disable
            WriteLiteral("        </tbody>\r\n    </table>\r\n</div>\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
