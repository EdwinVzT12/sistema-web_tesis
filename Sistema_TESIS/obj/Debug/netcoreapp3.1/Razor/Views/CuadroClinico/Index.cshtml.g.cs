#pragma checksum "C:\Users\cocad.EDWIN\Downloads\Sistema_TESIS\Sistema_TESIS\Views\CuadroClinico\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "fcaf1cb8cd1e663e12e3ac2a7565e1fbad69140d"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_CuadroClinico_Index), @"mvc.1.0.view", @"/Views/CuadroClinico/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\cocad.EDWIN\Downloads\Sistema_TESIS\Sistema_TESIS\Views\_ViewImports.cshtml"
using Sistema_TESIS;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\cocad.EDWIN\Downloads\Sistema_TESIS\Sistema_TESIS\Views\_ViewImports.cshtml"
using Sistema_TESIS.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"fcaf1cb8cd1e663e12e3ac2a7565e1fbad69140d", @"/Views/CuadroClinico/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"7017122e196eca4cb328ea36ceee3e28a042714d", @"/Views/_ViewImports.cshtml")]
    public class Views_CuadroClinico_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 2 "C:\Users\cocad.EDWIN\Downloads\Sistema_TESIS\Sistema_TESIS\Views\CuadroClinico\Index.cshtml"
  
    Layout = null;
    ViewData["Title"] = "Index";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<div class=\"col-xl-4 text-left my-3 mx-3\">\r\n    <h3 class=\"card-title blue-text font-weight-bold\">Seguimiento Clinico</h3>\r\n</div>\r\n<div class=\"col-xl-6 text-right my-3 mx-3\">\r\n    <a");
            BeginWriteAttribute("href", " href=\"", 248, "\"", 337, 2);
            WriteAttributeValue("", 255, "/CuadroClinico/Create?historiaClinicaId=", 255, 40, true);
#nullable restore
#line 11 "C:\Users\cocad.EDWIN\Downloads\Sistema_TESIS\Sistema_TESIS\Views\CuadroClinico\Index.cshtml"
WriteAttributeValue("", 295, ViewBag.HistoriaClinica.HistoriaClinicaId, 295, 42, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(@" class=""btn btn-outline-success""><i class=""fas fa-notes-medical""></i> Nueva ficha de seguimiento clínico</a>
</div>
<div class=""col-xl-12 my-3"">
    <table class=""table"">
        <thead class=""light-blue text-white"">
            <tr>
                <th scope=""col"">Fecha</th>
                <th scope=""col"">Evolucion</th>
                <th scope=""col"">Observaciones</th>
                <th scope=""col"">Opciones</th>
            </tr>
        </thead>
        <tbody>
");
#nullable restore
#line 24 "C:\Users\cocad.EDWIN\Downloads\Sistema_TESIS\Sistema_TESIS\Views\CuadroClinico\Index.cshtml"
             foreach (var item in Model)
            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                <tr>\r\n                    <th scope=\"row\">");
#nullable restore
#line 27 "C:\Users\cocad.EDWIN\Downloads\Sistema_TESIS\Sistema_TESIS\Views\CuadroClinico\Index.cshtml"
                               Write(item.Fecha.ToShortDateString());

#line default
#line hidden
#nullable disable
            WriteLiteral("</th>\r\n                    <td>");
#nullable restore
#line 28 "C:\Users\cocad.EDWIN\Downloads\Sistema_TESIS\Sistema_TESIS\Views\CuadroClinico\Index.cshtml"
                   Write(item.Evolucion);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                    <td>");
#nullable restore
#line 29 "C:\Users\cocad.EDWIN\Downloads\Sistema_TESIS\Sistema_TESIS\Views\CuadroClinico\Index.cshtml"
                   Write(item.Observaciones);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                    <td>\r\n                        <a");
            BeginWriteAttribute("href", " href=\"", 1124, "\"", 1174, 2);
            WriteAttributeValue("", 1131, "/CuadroClinico/Ver?id=", 1131, 22, true);
#nullable restore
#line 31 "C:\Users\cocad.EDWIN\Downloads\Sistema_TESIS\Sistema_TESIS\Views\CuadroClinico\Index.cshtml"
WriteAttributeValue("", 1153, item.CuadroClinicoId, 1153, 21, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" class=\"btn btn-outline-default\">Ver</a>\r\n                        <a");
            BeginWriteAttribute("href", " href=\"", 1243, "\"", 1296, 2);
            WriteAttributeValue("", 1250, "/CuadroClinico/Update?id=", 1250, 25, true);
#nullable restore
#line 32 "C:\Users\cocad.EDWIN\Downloads\Sistema_TESIS\Sistema_TESIS\Views\CuadroClinico\Index.cshtml"
WriteAttributeValue("", 1275, item.CuadroClinicoId, 1275, 21, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" class=\"btn btn-outline-default\">Actualizar</a>\r\n                    </td>\r\n                </tr>\r\n");
#nullable restore
#line 35 "C:\Users\cocad.EDWIN\Downloads\Sistema_TESIS\Sistema_TESIS\Views\CuadroClinico\Index.cshtml"
            }

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </tbody>\r\n    </table>\r\n</div>\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
